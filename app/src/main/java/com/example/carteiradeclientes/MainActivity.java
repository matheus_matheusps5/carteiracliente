package com.example.carteiradeclientes;

import android.content.Context;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.example.carteiradeclientes.database.DadosOpenHelper;
import com.example.carteiradeclientes.dominio.entidades.Cliente;
import com.example.carteiradeclientes.dominio.entidades.repositorio.ClienteRepositorio;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView lstDados;
    private FloatingActionButton fab;
    private ConstraintLayout layoutContentMain;

    private SQLiteDatabase conexao;

    private DadosOpenHelper dadosOpenHelper;

    private ClienteRepositorio clienteRepositorio;

    private ClienteAdapter clienteAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

         fab = findViewById(R.id.fab);
         lstDados =(RecyclerView)findViewById(R.id.lstDados);
         layoutContentMain = (ConstraintLayout)findViewById(R.id.layoutContentMain);

        criarConexao();





    }
    private void carregarDados(){

        lstDados.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        lstDados.setLayoutManager(linearLayoutManager);

        clienteRepositorio = new ClienteRepositorio(conexao);

        List<Cliente> dados = clienteRepositorio.buscarTodos();
        clienteAdapter = new ClienteAdapter(dados);
        Log.i("teste",clienteAdapter.toString());
        lstDados.setAdapter(clienteAdapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        carregarDados();
    }

    private void criarConexao(){

        try {

            dadosOpenHelper = new DadosOpenHelper(this);
            conexao = dadosOpenHelper.getWritableDatabase();
            Snackbar.make(layoutContentMain, R.string.menssage_conexao_bd,Snackbar.LENGTH_LONG).setAction("OK",null).show();

        }catch (SQLException ex){

            AlertDialog.Builder msg = new AlertDialog.Builder(this);
            msg.setTitle("Erro");
            msg.setMessage(ex.getMessage());
            msg.setNeutralButton("Ok",null);
            msg.show();

        }
    }

    public void cadastrar(View view){

        Intent it = new Intent(MainActivity.this,CadClienteActivity.class);
        startActivity(it);

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_activity_cad_cliente, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()) {

            case R.id.action_settings:
                Intent intent = new Intent(getApplicationContext(), PreferenciasUser.class);
                startActivity(intent);
                return true;

            case R.id.servidor:
                startActivity(new Intent(getApplicationContext(), ListaAlunosServerActivity.class));
                finish();
                return true;

            case R.id.sair:
                PreferenciasUser.setValuesBoolean(getApplicationContext(), "materConectado", false);
                startActivity(new Intent(MainActivity.this,LoginActivity.class));
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }



}
