package com.example.carteiradeclientes.database;

public class ScripDDL {

    public static String getCreateTableCliente(){

        StringBuilder sql = new StringBuilder();

        sql.append(" CREATE TABLE IF NOT EXISTS CLIENTE ( ");
        sql.append("    CODIGO INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " );
        sql.append("    NOME VARCHAR (200) NOT NULL DEFAULT '',");
        sql.append("    ENDERECO VARCHAR (200) NOT NULL DEFAULT '',");
        sql.append("    TELEFONE VARCHAR (11) NOT NULL DEFAULT '', ");
        sql.append("    EMAIL VARCHAR (200) NOT NULL DEFAULT '' ) ");

        return sql.toString();
    }

    public static String getCreateTableUsuario(){

        StringBuilder sql = new StringBuilder();

        sql.append(" CREATE TABLE IF NOT EXISTS USUARIO ( ");
        sql.append("    CODIGO INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " );
        sql.append("    USUARIO VARCHAR (200) NOT NULL DEFAULT '',");
        sql.append("    SENHA VARCHAR (200) NOT NULL DEFAULT '') ");


        return sql.toString();
    }
}
