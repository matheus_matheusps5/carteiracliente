package com.example.carteiradeclientes;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import com.example.carteiradeclientes.dominio.entidades.Conexao;
import com.example.carteiradeclientes.dominio.entidades.Usuario;
import com.example.carteiradeclientes.dominio.entidades.repositorio.UsuarioRepositorio;

public class CadUsuarioActivity extends AppCompatActivity {
    EditText user, senha;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cad_usuario);

        user = findViewById(R.id.user);
        senha = findViewById(R.id.senha);


    }

    public void cadastrar(View view) {
        CadClienteActivity cadClienteActivity = new CadClienteActivity();
        Conexao conexao = new Conexao(getApplicationContext());

        try {
            Usuario usuario = new Usuario();
            UsuarioRepositorio usuarioRepositorio = new UsuarioRepositorio(conexao.conectar());

            usuario.usuario = user.getText().toString();
            usuario.senha = senha.getText().toString();
            Long a = usuarioRepositorio.inserir(usuario);
            if(a != 0){
                startActivity(new Intent(CadUsuarioActivity.this,MainActivity.class));
                final CheckBox checkBox = findViewById(R.id.checkbox);
                if (checkBox.isChecked()) {
                    PreferenciasUser.setValuesBoolean(getApplicationContext(), "materConectado", true);
                } else {
                    PreferenciasUser.setValuesBoolean(getApplicationContext(), "materConectado", false);
                }
                finish();
            }
        }catch (Exception e){
            Log.i("erro",e.getMessage());
        }



    }
}
