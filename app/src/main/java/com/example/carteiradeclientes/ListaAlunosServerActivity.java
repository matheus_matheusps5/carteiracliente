package com.example.carteiradeclientes;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.example.carteiradeclientes.dominio.entidades.Cliente;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ListaAlunosServerActivity extends AppCompatActivity {

    ListView listViewAlunos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_alunos_server);

        listViewAlunos = findViewById(R.id.alunosServer);

        ArrayList<Cliente> al = (ArrayList<Cliente>) getAlunosServer(getApplicationContext());

        if(al != null) {

            ClienteServidorAdapter adapter = new ClienteServidorAdapter(ListaAlunosServerActivity.this, R.layout.item_lista_cliente, al);
            listViewAlunos.setAdapter(adapter);
        }


    }


    private static List<Cliente> parserJSON(Context context, String json) throws IOException {
        List<Cliente> alunos = new ArrayList<>();

        try {
            JSONObject root = new JSONObject(json);
            JSONObject obj = root.getJSONObject("alunos");
            JSONArray jsonAlunos = obj.getJSONArray("aluno");


            for (int i=0;i<jsonAlunos.length();i++) {
                Cliente a = new Cliente();
                a.nome = jsonAlunos.getJSONObject(i).getString("alunos_nome");
                a.email = jsonAlunos.getJSONObject(i).getString("alunos_email");
                alunos.add(a);
            }

        } catch (JSONException e) {
            throw new IOException(e.getMessage(), e);
        }
        return alunos;
    }

    public static List<Cliente> getAlunosServer(Context context) {

        try {
            String json = new GetAlunosTask().execute("http://45.55.53.18/aulamobile/webservices/alunos.json").get();
            return parserJSON(context, json);
        } catch (Exception e) {
            Log.i("erro", e.getMessage());
            return null;
        }
    }
}
