package com.example.carteiradeclientes;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.carteiradeclientes.R;
import com.example.carteiradeclientes.dominio.entidades.Cliente;

import java.util.List;

public class ClienteServidorAdapter extends ArrayAdapter<Cliente> {


        private List<Cliente> items;

        public ClienteServidorAdapter(Context context, int textViewResourceId, List<Cliente> items) {
            super(context, textViewResourceId, items);
            this.items = items;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                Context ctx = getContext();
                LayoutInflater vi = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.item_lista_cliente, null);
            }
            Cliente cliente = items.get(position);
            if (cliente != null) {
                ((TextView) v.findViewById(R.id.nome)).setText("Nome: " + cliente.nome);
                ((TextView) v.findViewById(R.id.email)).setText("Email: " + cliente.email);
            }
            return v;
        }

}
