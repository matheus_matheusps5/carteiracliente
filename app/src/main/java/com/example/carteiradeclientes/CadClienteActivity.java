package com.example.carteiradeclientes;

import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.carteiradeclientes.database.DadosOpenHelper;
import com.example.carteiradeclientes.dominio.entidades.Cliente;
import com.example.carteiradeclientes.dominio.entidades.Conexao;
import com.example.carteiradeclientes.dominio.entidades.repositorio.ClienteRepositorio;

public class CadClienteActivity extends AppCompatActivity {

    private EditText edtNome;
    private EditText edtEndereco;
    private EditText edtEmail;
    private EditText edtTelefone;

    private ConstraintLayout layoutContentActCadCliente;
    private ClienteRepositorio clienteRepositorio;

    private SQLiteDatabase conexao;

    private DadosOpenHelper dadosOpenHelper;

    private Cliente cliente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cad_cliente);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        edtNome = (EditText)findViewById(R.id.edtNome);
        edtEndereco = (EditText)findViewById(R.id.edtEndereco);
        edtEmail = (EditText)findViewById(R.id.edtEmail);
        edtTelefone = (EditText)findViewById(R.id.edtTelefone);

        layoutContentActCadCliente = (ConstraintLayout)findViewById(R.id.layoutContentActCadCliente);

        Conexao conexao = new Conexao(getApplicationContext());
        verificaParametro();

        Conexao c = new Conexao(getApplicationContext());
        clienteRepositorio = new ClienteRepositorio(c.conectar());


    }

    private void verificaParametro(){

        Bundle bundle = getIntent().getExtras();

        cliente = new Cliente();

        if ((bundle != null) && (bundle.containsKey("CLIENTE")) ){

            cliente = (Cliente) bundle.getSerializable("CLIENTE");

            edtNome.setText(cliente.nome);
            edtEndereco.setText(cliente.endereco);
            edtEmail.setText(cliente.email);
            edtTelefone.setText(cliente.telefone);
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater infleter = getMenuInflater();
        infleter.inflate(R.menu.menu_activity_cad_cliente, menu);
        return super.onCreateOptionsMenu(menu);
    }


    private void confirmar(){


        if (validaCampos() == false) {

            try {
                if (cliente.codigo ==0) {
                    clienteRepositorio.inserir(cliente);
                }else{

                    clienteRepositorio.alterar(cliente);
                }
                finish();

            } catch (SQLException ex) {

                AlertDialog.Builder msg = new AlertDialog.Builder(this);
                msg.setTitle("Erro");
                msg.setMessage(ex.getMessage());
                msg.setNeutralButton("Ok", null);
                msg.show();

            }
        }
    }

    private boolean validaCampos(){

        boolean res = false;

        String nome     = edtNome.getText().toString();
        String endereco = edtEndereco.getText().toString();
        String email    = edtEmail.getText().toString();
        String telefone = edtTelefone.getText().toString();


        cliente.nome     = nome;
        cliente.endereco = endereco;
        cliente.email    = email;
        cliente.telefone = telefone;



        if (res = isCampoVazio(nome)){
            edtNome.requestFocus();
        }else
            if (res = isCampoVazio(endereco)){
                edtEndereco.requestFocus();
            }else
                if (res = !isEmailValido(email)){
                    edtEmail.requestFocus();
                }else
                    if (res = isCampoVazio(telefone)){
                        edtTelefone.requestFocus();
                    }

        if (res){
            AlertDialog.Builder msg = new AlertDialog.Builder(this);
            msg.setTitle(R.string.title_aviso);
            msg.setMessage(R.string.menssage_campos_inalid);
            msg.setNeutralButton(R.string.title_menu_ok,null);
            msg.show();

        }
        return res;
    }

    private boolean isCampoVazio(String valor){

        boolean resultado = (TextUtils.isEmpty(valor)|| valor.trim().isEmpty() );
        return resultado;

    }

    private boolean isEmailValido(String email){

        boolean resultado = (!isCampoVazio(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches());
        return resultado;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        switch (id){

            case android.R.id.home:
                finish();
                break;

            case R.id.action_ok:

                confirmar();

                break;

            case R.id.action_excluir:

                clienteRepositorio.excluir(cliente.codigo);
                finish();

                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
