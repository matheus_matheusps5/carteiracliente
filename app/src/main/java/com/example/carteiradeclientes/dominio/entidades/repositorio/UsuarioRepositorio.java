package com.example.carteiradeclientes.dominio.entidades.repositorio;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.carteiradeclientes.LoginActivity;
import com.example.carteiradeclientes.dominio.entidades.Cliente;
import com.example.carteiradeclientes.dominio.entidades.Usuario;

import java.util.ArrayList;
import java.util.List;

public class UsuarioRepositorio {

    SQLiteDatabase conexao;


    public UsuarioRepositorio(SQLiteDatabase conexao){

        this.conexao = conexao;
    }


    public Long inserir(Usuario usuario){

        try {

            ContentValues contentValues = new ContentValues();
            contentValues.put("USUARIO",usuario.usuario);
            contentValues.put("SENHA",usuario.senha);


            return  conexao.insertOrThrow("USUARIO",null,contentValues);

        }catch (Exception ex){
            Log.i("erro",ex.getMessage());
            return null;
        }

    }

    public Usuario logar(Usuario usuario){

        Log.i("u",usuario.usuario);

        StringBuilder sql = new StringBuilder();
        sql.append("SELECT * FROM USUARIO WHERE USUARIO=? AND SENHA=?;");

        try {
            Cursor resultado = conexao.rawQuery(sql.toString(),new String[]{usuario.usuario,usuario.senha});

            if (resultado.getCount() > 0 ) {
                resultado.moveToFirst();
                do {

                    Usuario user = new Usuario();
                    user.cpdigo = resultado.getInt( resultado.getColumnIndexOrThrow("CODIGO"));
                    user.usuario = resultado.getString( resultado.getColumnIndexOrThrow("USUARIO"));
                    user.senha = resultado.getString( resultado.getColumnIndexOrThrow("SENHA"));

                    Log.i("aff",user.usuario);
                    return  user;

                }while (resultado.moveToNext());
            }
        }catch (Exception e){
            Log.i("erro",e.getMessage());
            return null;
        }
        return null;
    }


}
