package com.example.carteiradeclientes.dominio.entidades;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.example.carteiradeclientes.R;
import com.example.carteiradeclientes.database.DadosOpenHelper;
import com.example.carteiradeclientes.dominio.entidades.repositorio.ClienteRepositorio;

public class Conexao {
    private Context context;
    public Conexao(Context context){
        this.context = context;
    }
    public SQLiteDatabase conectar(){

        try {


            DadosOpenHelper dadosOpenHelper = new DadosOpenHelper(this.context);
            SQLiteDatabase conexao = dadosOpenHelper.getWritableDatabase();

            return conexao;

        }catch (SQLException ex){
            Log.i("ERRO",ex.getMessage());
            return null;
        }
    }
}
