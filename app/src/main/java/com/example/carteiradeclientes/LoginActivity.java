package com.example.carteiradeclientes;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.carteiradeclientes.dominio.entidades.Conexao;
import com.example.carteiradeclientes.dominio.entidades.Usuario;
import com.example.carteiradeclientes.dominio.entidades.repositorio.UsuarioRepositorio;

public class LoginActivity extends AppCompatActivity {


    EditText user,senha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        boolean logado =  PreferenciasUser.getValuesBoolean(getApplicationContext(), "materConectado");

        Log.i("asd",String.valueOf(logado));
        user = findViewById(R.id.campoUsuario);
        senha = findViewById(R.id.campoSenha);

        if(logado){
            startActivity(new Intent(LoginActivity.this,MainActivity.class));
            finish();
        }

    }

    public void logar(View view){
        Conexao c = new Conexao(getApplicationContext());
        UsuarioRepositorio u = new UsuarioRepositorio(c.conectar());
        Usuario usuario = new Usuario();
        usuario.senha = senha.getText().toString();
        usuario.usuario = user.getText().toString();

        if( u.logar(usuario) != null){

            final CheckBox checkBox = findViewById(R.id.checkbox);
            startActivity(new Intent(LoginActivity.this,MainActivity.class));
            if (checkBox.isChecked()) {
                PreferenciasUser.setValuesBoolean(getApplicationContext(), "materConectado", true);
            } else {
                PreferenciasUser.setValuesBoolean(getApplicationContext(), "materConectado", false);
            }
            finish();
        }else{
            Toast.makeText(getApplicationContext(),"Usuário não cadastrado",Toast.LENGTH_SHORT).show();
        }
    }


    public void registrar(View view) {

        startActivity( new Intent(LoginActivity.this,CadUsuarioActivity.class));
        finish();
    }
}
